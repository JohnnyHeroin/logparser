# -*- coding: utf-8 -*-
from os.path import join

MAX_LINES = 100
MIN_LINES = 100


def open_logfile(logpath):
    """ Read log file and return lines.
        :param logpath: path to log file
        :return: list of lines
    """
    with open(logpath, 'r') as logfile:
        return logfile.readlines()


def get_current_lines(logfile, log_id):
    """ Search `log_id` in log file and return lines.
        :param logfile: list of lines
        :param log_id: log id
        :return: list of lines
    """
    last_line = len(logfile) - 1
    penultimate_lines = last_line - MAX_LINES

    for number, line in enumerate(logfile):

        if log_id in line:

            if number <= MAX_LINES:
                return logfile[:number + MAX_LINES]

            elif number == last_line or number >= penultimate_lines:
                return logfile[number - MAX_LINES:]

            else:
                return logfile[number - MIN_LINES: MAX_LINES + number]


def print_lines(path, mask, logid):
    """ Print lines.
        :param path: path to log folder
        :param mask: mask log file
        :param logid: log id
    """
    logs = open_logfile(join(path, mask))
    print(''.join(get_current_lines(logs, logid)))
