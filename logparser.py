# -*- coding: utf-8 -*-
import argparse
from parser import print_lines


parser = argparse.ArgumentParser(description='Search log id in log file.')
parser.add_argument('-p, --path', action='store', dest='path', help='Path to log files')
parser.add_argument('-m, --mask', action='store', dest='mask', help='File name mask')
parser.add_argument('-i, --id', action='store', dest='id', help='Log ID')
args = parser.parse_args()

print_lines(args.path, args.mask, args.id)
