About
-----
Log parser

Requirements
------------
Python 3.5

Usage
-----

    $ python3.5 logparse.py -p <path to log folder> -m <file mask> -i <log id>

Example
-------

    $  python3.5 logparser.py -p "~/path/to/log/" -m "example.log" -i "bdcf21a761708d671378bf8673b2ffb1"